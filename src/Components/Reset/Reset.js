import React from 'react';

const resetGame = (setBoard) => {
    const ringIndex = Math.floor(Math.random() * 36)
    const arrayCell =  [...new Array(36)]
        .map(() => 1);
    setBoard({array:arrayCell,index: ringIndex,tryCount: 35 })
}
const Reset = props => {
    return (
        <button onClick={() => resetGame(props.setBoard) }>Reset</button>
    );
};

export default Reset