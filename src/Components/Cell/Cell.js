import React from 'react';

const changeColor = (index,arrayCellIndex,arrayCellValue) => {
    let colorCell = ['cell'];
    if (arrayCellValue === 0 && arrayCellIndex === index) {
        colorCell.push('cellRing');
      } else if(arrayCellValue === 0) {
        colorCell.push('cellWhite');
    }
    return colorCell.join(' ');
}

const openCell = (arrayCellIndex,arrayCellValue,setVal) => {
    if (arrayCellValue === 1 ) {
        setVal(arrayCellIndex,0);
    }
}

const Cell = props => {
    return (
        <div className={changeColor(props.ringIndex, props.arrayCellIndex, props.arrayCellValue, props.finishMessage)} onClick={() =>openCell( props.arrayCellIndex, props.arrayCellValue,props.SetCellValue)}>

        </div>
    );
};

export default Cell