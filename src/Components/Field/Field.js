import React from 'react';
import Cell from "../Cell/Cell";
import './Field.css'

const Field = ({board, SetCellValue}) => {
    const cellsBoard = board.array.map((cell, index) => {
        return <Cell
            SetCellValue={(index,value)=>SetCellValue(index,value)}
            ringIndex={board.index}
            arrayCellIndex={index}
            arrayCellValue={board.array[index]}/>
    });

    return (
        <div className='wrapper'>
            {cellsBoard}
        </div>
    );
};

export default Field;