import React from 'react';

const Counter = props => {
    let counterStyle = {color: 'green'};

    if (props.children < 5) {
        counterStyle.color = 'red';
    }

    return (
        <p style={counterStyle}>
            Tries: {props.children}
        </p>

    );
};

export default Counter