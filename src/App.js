import React, {useState} from 'react';
import './App.css';
import "./Components/Cell/Cell.css";
import Reset from "./Components/Reset/Reset";
import Counter from "./Components/Counter/Counter";
import Field from "./Components/Field/Field";

function App() {
    const arrayCell = [...new Array(36)]
        .map(() => 1);
    const ringIndex = Math.floor(Math.random() * 36)
    const [board, setBoard] = useState({array: arrayCell, index: ringIndex, tryCount:35});

    function SetCellValue(index, value) {
        setBoard({
            ...board,tryCount: board.tryCount-1,
            array:board.array.map((elem,i) => {
                if(index===i) return value;
                else return elem;}
                )
        })
    }

    return (
    <div className="App">
        <h1 className="header">Game</h1>
        <Field board={board} SetCellValue={SetCellValue}/>
        <Counter>{board.tryCount}</Counter>
        <Reset setBoard={setBoard}>Reset</Reset>
    </div>
    );
}
export default App;
